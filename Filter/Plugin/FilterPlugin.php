<?php

namespace HexBrain\Filter\Plugin;

class FilterPlugin
{
    public function aroundAddFilter(\Magento\Catalog\Ui\DataProvider\Product\ProductDataProvider $dataProvider, callable $proceed, \Magento\Framework\Api\Filter $filter)
    {
        if ($filter->getField() == "sku" && count(explode(",", str_replace("%", "", $filter->getValue()))) > 1) {
            $withComma = explode(",", str_replace("%", "", $filter->getValue()));
            $attrs = [];
            foreach ($withComma as $cItem) {
                $attrs[] = ['attribute' => $filter->getField(), $filter->getConditionType() => '%' . trim($cItem) . '%'];
            }
            $dataProvider->getCollection()->addAttributeToFilter($attrs);
        } else {
            $proceed($filter);
        }
    }
}
